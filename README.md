[![pipeline status](https://git.sw4j.net/osdevelopment-info/sys-emu/badges/master/pipeline.svg)](https://git.sw4j.net/osdevelopment-info/sys-emu/pipelines)

# SYS-EMU

A system emulator to emulate different computer systems. The major use of this emulator should be to support tutorials.

A description is available at the [project page](https://sys-emu.osdevelopment.info/).
