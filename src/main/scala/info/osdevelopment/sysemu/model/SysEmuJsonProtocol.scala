/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsString, JsValue, RootJsonFormat, _}

trait SysEmuJsonProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object SystemJsonFormat extends RootJsonFormat[SystemModel] {

    override def read(json: JsValue): SystemModel = {
      deserializationError("Deserialization is not supported for Systems")
      /*json.asJsObject.getFields("uuid") match {
        case Seq(JsString(uuid)) =>
          new System(Some(UUID.fromString(uuid)))
      }*/
    }

    override def write(obj: SystemModel): JsValue = {
      JsObject(("uuid", JsString(obj.uuid.toString)))
    }

  }

  implicit object ProcessorJsonFormat extends RootJsonFormat[ProcessorModel] {

    override def read(json: JsValue): ProcessorModel = {
      deserializationError("Deserialization is not supported for Processors")
    }

    override def write(obj: ProcessorModel): JsValue = {
      JsObject(
        ("name", JsString(obj.name)),
        ("maxMemory", JsNumber(obj.maxMemory))
      )
    }

  }

  implicit object MessageJsonFormat extends RootJsonFormat[MessageModel] {

    override def read(json: JsValue): MessageModel = {
      deserializationError("Deserialization is not supported for Message")
    }

    override def write(obj: MessageModel): JsValue = {
      JsObject(
        ("message", JsString(obj.message))
      )
    }

  }

}
