/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018  Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package info.osdevelopment.sysemu.model

import info.osdevelopment.sysemu.processor.ProcessorDescriptor
import io.swagger.v3.oas.annotations.media.Schema

object ProcessorModel {

  def apply(processorDescriptor: ProcessorDescriptor) =
    new ProcessorModel(processorDescriptor.name, processorDescriptor.maxMemory)

}

@Schema(
  name = "Processor"
)
class ProcessorModel(@Schema(example = "8086") val name: String,
                     @Schema(example = "1048576") val maxMemory: Long) {

}
