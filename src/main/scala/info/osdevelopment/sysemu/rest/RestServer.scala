/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.rest

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.event.slf4j.SLF4JLogging
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import info.osdevelopment.sysemu.actors.SystemRepositoryActor
import info.osdevelopment.sysemu.config.Configuration
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.{Contact, Info, License}

import scala.concurrent.{ExecutionContextExecutor, Future}

@OpenAPIDefinition(
  info =  new Info(
    title = "sys-emu",
    version = "0.1",
    description = "Remote interface for sys-emu",
    license = new License(name = "GNU AGPLv3", url = "https://www.gnu.org/licenses/agpl.html"),
    contact = new Contact(url = "https://sys-emu.osdevelopment.info")
  )
)
class RestServer(implicit val actorSystem: ActorSystem, val config: Configuration) extends SLF4JLogging {

  object ShutdownActor {

    def props(): Props = Props(new ShutdownActor())

  }

  class ShutdownActor extends Actor with ActorLogging {

    override def receive: Actor.Receive = {
      case "shutdown" => shutdown()
    }

  }

  var bindingFuture: Option[Future[ServerBinding]] = None

  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = actorSystem.dispatcher
  val shutdownActor = actorSystem.actorOf(ShutdownActor.props())
  val systemRepositoryActor = actorSystem.actorOf(SystemRepositoryActor.props(), "system-repository")

  def start(): Unit = {
    bindingFuture match {
      case None => bindingFuture = Some(Http().bindAndHandle(this.route, config.serviceHost, config.servicePort))
      case _ => log.warn("Tried to start an already running server")
    }
  }

  def shutdown(): Unit = {
    bindingFuture match {
      case Some(future) =>
        future.flatMap(_.unbind())
        actorSystem.terminate()
      case None => log.warn("Tried to shutdown a not running server")
    }
  }

  def route: Route = {
    new RestProcessorService().route ~
    new RestSystemService(systemRepositoryActor).route ~
    SwaggerService.routes ~
    path("swagger-ui") {
      getFromResource("swagger-ui/index.html")
    } ~
    getFromResourceDirectory("swagger-ui") ~
    path("shutdown") {
      post {
        shutdownActor ! "shutdown"
        complete(StatusCodes.OK)
      }
    }
  }

}
