/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.rest

import akka.actor.ActorRef
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Location, `Content-Type`}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern._
import info.osdevelopment.sysemu.model.{MessageModel, SysEmuJsonProtocol, SystemModel}
import info.osdevelopment.sysemu.system.Systems
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import java.util.UUID
import info.osdevelopment.sysemu.actors.SystemRepositoryActor.CreateSystem
import javax.ws.rs.{DELETE, GET, POST, Path}
import org.slf4j.{Logger, LoggerFactory}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import spray.json.{JsObject, JsString}

@Path("/systems")
class RestSystemService(val systemRepositoryActor: ActorRef) extends SysEmuJsonProtocol {

  val log: Logger = LoggerFactory getLogger classOf[RestSystemService]

  def route: Route = {
    respondWithHeader(`Content-Type`(MediaTypes.`application/json`))
    pathPrefix("systems") {
      getSystem ~
      deleteSystem ~
      getAllSystems ~
      createSystem
    }
  }

  @GET
  @Path("")
  @Operation(
    summary = "Get all systems",
    description = "Return all systems available",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "All systems",
        content = Array(
          new Content(
            array = new ArraySchema(
              schema = new Schema(implementation = classOf[SystemModel])
            ),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def getAllSystems: Route = {
    pathEndOrSingleSlash {
      get {
        val systems = Systems.all().map(system => SystemModel(system.uuid))
        complete(StatusCodes.OK, systems)
      }
    }
  }

  @POST
  @Path("")
  @Operation(
    summary = "Create a new system",
    description = "Create a new system and return the created UUID of the system",
    responses = Array(
      new ApiResponse(
        responseCode = "201",
        description = "System created",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[SystemModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def createSystem: Route = {
    pathEndOrSingleSlash {
      post {
        extractUri { uri =>
          val eventualUuid = (systemRepositoryActor ? CreateSystem)(15.seconds).mapTo[UUID]
//          val system = Some(new System(uuid))
//          Systems.add(system)
//          systemRepositoryActor ! CreateSystem
          val uuid = Await.result(eventualUuid, 15.seconds)
          val uuidPath = "/" + uuid
          complete(StatusCodes.Created,
            List(Location(uri.copy(path = uri.path + uuidPath))),
            SystemModel(uuid))
        }
      }
    }
  }

  @GET
  @Path("/{uuid}")
  @Operation(
    summary = "Return a specific system",
    description = "Search for the system specified by the UUID in the path and return it",
    parameters = Array(
      new Parameter(
        in = ParameterIn.PATH,
        name = "uuid",
        schema = new Schema(
          name = "uuid",
          description = "The UUID of the system to retrieve",
          `type` = "uuid",
          format = "uuid",
          example = "01234567-89ab-cdef-0123-456789abcdef"
        )
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "The system requested",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[SystemModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "404",
        description = "When the system with the given UUID cannot be found",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[MessageModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def getSystem: Route = {
    path(JavaUUID) { uuid =>
      get {
        Systems.byUUID(uuid) match {
          case Some(system) => complete(StatusCodes.OK, SystemModel(system.uuid))
          case None => complete(StatusCodes.NotFound, new MessageModel("No system with UUID " + uuid + " can be found."))
        }
      }
    }
  }

  @DELETE
  @Path("/{uuid}")
  @Operation(
    summary = "Delete a specific system",
    description = "Search for the system specified by the UUID in the path and delete it",
    parameters = Array(
      new Parameter(
        in = ParameterIn.PATH,
        name = "uuid",
        schema = new Schema(
          name = "uuid",
          description = "The UUID of the system to delete",
          `type` = "uuid",
          format = "uuid",
          example = "01234567-89ab-cdef-0123-456789abcdef"
        )
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "202",
        description = "System deleted",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[MessageModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "404",
        description = "When the system with the given UUID cannot be found",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[MessageModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def deleteSystem: Route = {
    path(JavaUUID) { uuid =>
      delete {
        if (Systems.remove(uuid)) {
          complete(StatusCodes.Accepted,
            JsObject(("message", JsString("Removed system with UUID " + uuid + "."))))
        } else {
          complete(StatusCodes.NotFound,new MessageModel("No system with UUID " + uuid + " can be found."))
        }
      }
    }
  }

}
