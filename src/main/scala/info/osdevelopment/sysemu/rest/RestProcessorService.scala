/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.rest

import akka.http.scaladsl.model.headers.`Content-Type`
import akka.http.scaladsl.model.{MediaTypes, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import info.osdevelopment.sysemu.processor.ProcessorDescriptor
import info.osdevelopment.sysemu.model.{MessageModel, ProcessorModel, SysEmuJsonProtocol}
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.{GET, Path}
import org.slf4j.{Logger, LoggerFactory}

@Path("/processors")
class RestProcessorService extends SysEmuJsonProtocol {

  val log: Logger = LoggerFactory getLogger classOf[RestProcessorService]

  def route: Route = {
    respondWithHeader(`Content-Type`(MediaTypes.`application/json`))
    pathPrefix("processors") {
      getAllProcessors ~
      getProcessor
    }
  }

  @GET
  @Path("")
  @Operation(
    summary = "Get all processors",
    description = "Return all processors available",
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "All processors",
        content = Array(
          new Content(
            array = new ArraySchema(
              schema = new Schema(
                implementation = classOf[String],
                example = "8086")
            ),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def getAllProcessors: Route = {
    pathEndOrSingleSlash {
      get {
        val processorDescriptors = ProcessorDescriptor loadProcessors()
        complete(StatusCodes.OK, processorDescriptors.map(d => d.name))
      }
    }
  }

  @GET
  @Path("/{name}")
  @Operation(
    summary = "Return a specific processor",
    description = "Search for the processor specified by the name in the path and return it",
    parameters = Array(
      new Parameter(
        in = ParameterIn.PATH,
        name = "name",
        schema = new Schema(
          name = "name",
          description = "The name of the processor to retrieve",
          `type` = "string",
          example = "8086"
        )
      )
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "The processor requested",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[ProcessorModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "404",
        description = "When the processor with the given name cannot be found",
        content = Array(
          new Content(
            schema = new Schema(implementation = classOf[MessageModel]),
            mediaType = "application/json"
          )
        )
      ),
      new ApiResponse(
        responseCode = "500",
        description = "Internal server error"
      )
    )
  )
  def getProcessor: Route = {
    pathPrefix(Segment) { name =>
      get {
        val processorDescriptor = ProcessorDescriptor loadProcessor name
        processorDescriptor match {
          case Some(descriptor) => complete(StatusCodes.OK, ProcessorModel(descriptor))
          case None =>
            complete(StatusCodes.NotFound, new MessageModel("No processor with name " + name + " can be found."))
        }
      }
    }
  }

}
