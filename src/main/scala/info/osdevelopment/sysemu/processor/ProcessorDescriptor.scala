/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package info.osdevelopment.sysemu.processor

import java.util.ServiceLoader

import scala.collection.JavaConverters._

object ProcessorDescriptor {

  def loadProcessors(): Iterable[ProcessorDescriptor] = {
    ServiceLoader.load(classOf[ProcessorDescriptor]).asScala
  }

  def loadProcessor(name: String): Option[ProcessorDescriptor] = {
    ServiceLoader.load(classOf[ProcessorDescriptor]).asScala.find(_.name == name)
  }

}

/**
  * <p>An abstract description of a processor usable in a system. This descriptor only contains the very basic data of
  * the processor and offers a possibility to create a processor to be added to a system.</p>
  * <p>This class is used to find automatically new processor descriptions via a ServiceLoader.</p>
  * @param name the name of the processor (e.g. "8086" or "68000").
  * @param maxMemory the maximum memory that this processor can access.
  * @param romName the proposed resource name for a ROM to be used by the processor.
  */
abstract class ProcessorDescriptor (val name: String,
                                    val maxMemory: Long,
                                    val romName: String) {

  def createProcessor: Processor

}
