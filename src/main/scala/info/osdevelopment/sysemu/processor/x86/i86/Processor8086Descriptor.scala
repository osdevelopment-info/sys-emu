/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018  Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package info.osdevelopment.sysemu.processor.x86.i86

import info.osdevelopment.sysemu.processor.{Processor, ProcessorDescriptor}
import info.osdevelopment.sysemu.support.Utilities._

class Processor8086Descriptor extends ProcessorDescriptor("8086", 1.Mi, "bios86") {

  override def createProcessor: Processor = new Processor8086(this)

}
