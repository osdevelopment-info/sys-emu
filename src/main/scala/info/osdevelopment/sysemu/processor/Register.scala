/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.processor

/**
  * Defines a register including a name, a content and a size. The size is optional. This class should be used to ask a
  * processor for registers including content and to set the register content of a processor. Processors should always
  * set the size whereas external users should leave it empty.
  *
  * @param core the core to which the given register belongs (starting with 0)
  * @param name the name of the register, e.g. "A" or "SI"
  * @param content the current content of the register or the value that should be set to the register
  * @param size the size of the register in bits. Should always be set by processors.
  */
final class Register(val core: Int, val name: String, val content: BigInt, val size: Int = 0) {

}
