/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu


import akka.actor.{ActorSystem, Props}
import info.osdevelopment.sysemu.config.Configuration
import info.osdevelopment.sysemu.rest.RestServer

object Main {

  def main(args: Array[String]): Unit = {
    new Main run args
  }

}

class Main {

  def run(args: Array[String]): Unit = {
    implicit val configuration = new Configuration {}
    implicit val actorSystem = ActorSystem("emu-system", configuration.config)
    val httpRouter = new RestServer()
    httpRouter.start()
  }

}
