/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018  Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package info.osdevelopment.sysemu.actors

import akka.actor.{Actor, ActorLogging, Props}
import info.osdevelopment.sysemu.actors.SystemActor.Create
import java.util.UUID

object SystemActor {

  case object Create

  def props(uuid: UUID): Props = Props(new SystemActor(UUID.randomUUID()))

}

class SystemActor(val uuid: UUID) extends Actor with ActorLogging {

  override def receive: Receive = {
    case Create => log.info("Created system " + uuid)
  }

}
