/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package info.osdevelopment.sysemu.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import info.osdevelopment.sysemu.actors.SystemRepositoryActor.CreateSystem
import info.osdevelopment.sysemu.system.System
import java.util.UUID
import scala.collection.mutable

object SystemRepositoryActor {

  case object CreateSystem

  def props(): Props = Props(new SystemRepositoryActor())

}

class SystemRepositoryActor extends Actor with ActorLogging {

  /** All known systems. */
  private val systemMap = mutable.Map[UUID, ActorRef]()

  override def receive: Actor.Receive = {
    case CreateSystem => sender() ! createSystem()
  }

  def createSystem(): UUID = {
    val uuid = UUID.randomUUID()
    val system = context.actorOf(SystemActor.props(uuid), uuid.toString())
    systemMap += (uuid -> system)
    log info("Created system with uuid " + uuid)
    return uuid
  }

}
