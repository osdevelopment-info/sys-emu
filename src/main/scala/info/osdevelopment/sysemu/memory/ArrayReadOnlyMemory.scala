/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.memory

import scala.util.Try

/**
  * A read-only memory backed by an array.
 *
  * @param data the data for the read-only memory.
  */
class ArrayReadOnlyMemory (val data: Array[Byte]) extends ReadOnlyMemory {

  /**
    * Returns the size of the memory.
    * @return the size of the memory
    */
  override def size: Long = data.length

  /**
    * The implementation of the read.
    * @param address the address that should be read
    * @return a `Success` with the byte read at the given address or a `Failure`
    */
  override protected def doRead(address: Long): Try[Byte] = {
    Try(data(address.asInstanceOf[Int]))
  }

}
