/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.config

import com.typesafe.config.{Config, ConfigFactory}
import scala.util.Try

/**
  * The global configuration of the application, read from `application.conf`.
  */
trait Configuration {

  /**
    * The config read from the file.
    */
  val config: Config = ConfigFactory.load

  /**
    * The service host read from the config file. By default `localhost` is used.
    */
  lazy val serviceHost: String = Try(config.getString("service.host")).getOrElse("localhost")

  /**
    * The service port read from the config file. By default `8080` is used.
    */
  lazy val servicePort: Int = Try(config.getInt("service.port")).getOrElse(8080)

}
