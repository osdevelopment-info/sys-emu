---
layout: default
---

# Introduction

This project is for creating a system emulator that emulates different systems e.g. a PC with 8086 µProcessor.

# Developer Documentation

* [API documentation](api/info/osdevelopment/sysemu/)
* [Coverage Report](scoverage-report/)
