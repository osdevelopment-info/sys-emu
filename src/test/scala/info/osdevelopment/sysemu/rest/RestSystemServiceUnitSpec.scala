/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.testkit.Specs2RouteTest
import akka.testkit.TestKit
import info.osdevelopment.sysemu.actors.SystemRepositoryActor
import org.specs2.mutable
import org.specs2.specification.AfterAll

class RestSystemServiceUnitSpec() extends mutable.SpecificationLike with Specs2RouteTest with AfterAll {

  override def afterAll = TestKit.shutdownActorSystem(system)

  "A RestSystemService" >> {
    "with a GET on /systems" >> {
      "should return 200/OK" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Get("/systems") ~> service.route ~> check {
          status must_== StatusCodes.OK
        }
      }
      "should return content type 'application/json'" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Get("/systems") ~> service.route ~> check {
          contentType must_== ContentTypes.`application/json`
        }
      }
      "should return a JSON array" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Get("/systems") ~> service.route ~> check {
          responseAs[String] must be matching "\\[.*\\]"
        }
      }
    }
    "with a POST on /systems" >> {
      "should return a 201/CREATED" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Post("/systems") ~> service.route ~> check {
          status must_== StatusCodes.Created
        }
      }
      "should return content type 'application/json'" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Post("/systems") ~> service.route ~> check {
          contentType must_== ContentTypes.`application/json`
        }
      }
      "should return a JSON object with a UUID" >> {
        val repoActor = system.actorOf(SystemRepositoryActor.props())
        val service = new RestSystemService(repoActor)
        Post("/systems") ~> service.route ~> check {
          responseAs[String] must be matching "\\{\\s*\"uuid\"\\s*:" +
            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}"
        }
      }
    }
    "with a GET on /<uuid>" >> {
      "and an invalid UUID" >> {
        "should return a 404/NOT FOUND" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Get("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            status must_== StatusCodes.NotFound
          }
        }
        "should return content type 'application/json'" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Get("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            contentType must_== ContentTypes.`application/json`
          }
        }
        "should return a JSON object with a message" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Get("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            responseAs[String] must be matching "\\{\\s*\"message\"\\s*:\\s*\".*\".*\\}"
          }
        }
      }
      "and a valid UUID" >> {
        // TODO reenable test
//        "should return a 200/OK" >> {
//          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
//            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
//          val repoActor = system.actorOf(SystemRepositoryActor.props())
//          val service = new RestSystemService(repoActor)
//          var responseUuid: String = ""
//          Post("/systems") ~> service.route ~> check {
//            responseAs[String] match {
//              case uuidPattern(uuid) => responseUuid = uuid
//              case _ => failure
//            }
//          }
//          Get("/systems/" + responseUuid) ~> service.route ~> check {
//            status must_== StatusCodes.OK
//          }
//        }
        "should return content type 'application/json'" >> {
          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          var responseUuid: String = ""
          Post("/systems") ~> service.route ~> check {
            responseAs[String] match {
              case uuidPattern(uuid) => responseUuid = uuid
              case _ => failure
            }
          }
          Get("/systems/" + responseUuid) ~> service.route ~> check {
            contentType must_== ContentTypes.`application/json`
          }
        }
        // TODO reenable test
//        "should return a JSON object with the same UUID as requested" >> {
//          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
//            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
//          val repoActor = system.actorOf(SystemRepositoryActor.props())
//          val service = new RestSystemService(repoActor)
//          var responseUuid: String = ""
//          Post("/systems") ~> service.route ~> check {
//            responseAs[String] match {
//              case uuidPattern(uuid) => responseUuid = uuid
//              case _ => failure
//            }
//          }
//          Get("/systems/" + responseUuid) ~> service.route ~> check {
//            responseAs[String] must be matching "\\{\\s*\"uuid\"\\s*:\\s*\"" + responseUuid + "\".*\\}"
//          }
//        }
      }
    }
    "with a DELETE on /<uuid>" >> {
      "and an invalid UUID" >> {
        "should return a 404/NOT FOUND" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Delete("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            status must_== StatusCodes.NotFound
          }
        }
        "should return content type 'application/json'" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Delete("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            contentType must_== ContentTypes.`application/json`
          }
        }
        "should return a JSON object with a message" >> {
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          Delete("/systems/00000000-0000-0000-0000-000000000000") ~> service.route ~> check {
            responseAs[String] must be matching "\\{\\s*\"message\"\\s*:\\s*\".*\".*\\}"
          }
        }
      }
      "and a valid UUID" >> {
        // TODO reenable test
//        "should return a 202/ACCEPTED" >> {
//          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
//            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
//          val repoActor = system.actorOf(SystemRepositoryActor.props())
//          val service = new RestSystemService(repoActor)
//          var responseUuid: String = ""
//          Post("/systems") ~> service.route ~> check {
//            responseAs[String] match {
//              case uuidPattern(uuid) => responseUuid = uuid
//              case _ => failure
//            }
//          }
//          Delete("/systems/" + responseUuid) ~> service.route ~> check {
//            status must_== StatusCodes.Accepted
//          }
//        }
        "should return content type 'application/json'" >> {
          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          var responseUuid: String = ""
          Post("/systems") ~> service.route ~> check {
            responseAs[String] match {
              case uuidPattern(uuid) => responseUuid = uuid
              case _ => failure
            }
          }
          Delete("/systems/" + responseUuid) ~> service.route ~> check {
            contentType must_== ContentTypes.`application/json`
          }
        }
        "should return a JSON object with a message" >> {
          val uuidPattern = ("\\{\\s*\"uuid\"\\s*:" +
            "\\s*\"(\\p{XDigit}{8}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{4}-\\p{XDigit}{12})\".*\\}").r
          val repoActor = system.actorOf(SystemRepositoryActor.props())
          val service = new RestSystemService(repoActor)
          var responseUuid: String = ""
          Post("/systems") ~> service.route ~> check {
            responseAs[String] match {
              case uuidPattern(uuid) => responseUuid = uuid
              case _ => failure
            }
          }
          Delete("/systems/" + responseUuid) ~> service.route ~> check {
            responseAs[String] must be matching "\\{\\s*\"message\"\\s*:\\s*\".*\".*\\}"
          }
        }
      }
    }
  }

}
