/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.rest

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.testkit.Specs2RouteTest
import info.osdevelopment.sysemu.config.Configuration
import org.specs2._

class RestServerUnitSpec extends mutable.SpecificationLike with Specs2RouteTest {

  class TestConfig extends Configuration

  implicit val config = new TestConfig()
  val server = new RestServer()

  "A RestService" >> {
    "with a GET at /systems" >> {
      "should return a JSON array from RestSystemService" >> {
        //val service = new RestServer(system, this)
        Get("/systems") ~> server.route ~> check {
          responseAs[String] must be matching "\\[.*\\]"
        }
      }
    }
    "with a POST at /shutdown" >> {
      "should return 200/OK" >> {
        Post("/shutdown") ~> server.route ~> check {
          status must_== StatusCodes.OK
        }
      }
    }
    "with a GET on /processors" >> {
      "should return 200/OK" >> {
        Get("/processors") ~> server.route ~> check {
          status must_== StatusCodes.OK
        }
      }
    }
  }

}
