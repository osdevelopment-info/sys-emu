/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.processor.test

import info.osdevelopment.sysemu.processor.{Processor, ProcessorDescriptor, Register}
import scala.util.{Failure, Try}

/**
  * A processor used for unit testing.
  */
class TestProcessor (override val descriptor: ProcessorDescriptor) extends Processor (descriptor) {

  /**
    * All registers of all cores when the cores are stopped.
    *
    * The result is Success if the cores can return the registers. When Failure is returned then the cores
    * cannot return the registers at the moment, e.g. because any is running at the moment.
    *
    * The key of the map is the register name.
    *
    * @return A Try containing a Map with the core as key and a Map with the register name as key and the register as
    *         value.
    */
  override def registers: Try[Map[Int, Map[String, Register]]] = {
    Failure(new UnsupportedOperationException("not supported in TestProcessor"))
  }

  /**
    * All registers of a core when the core is stopped.
    *
    * The result is Success if the core can return the registers. When Failure is returned then the core
    * cannot return the registers at the moment, e.g. because it is running at the moment.
    *
    * The key of the map is the register name.
    *
    * @param core the core for which the registers should be queried
    * @return A Try containing a Map with the register name as key and the register as value.
    */
  override def registers(core: Int): Try[Map[String, Register]] = {
    Failure(new UnsupportedOperationException("not supported in TestProcessor"))
  }

  /**
    * Return the register with the given name of the given core, e.g. "AX" or "D1".
    *
    * @param core the core for which the register should be returned
    * @param name the name of the register to return
    * @return the register named by name. Failure if the register does not exist or cannot be returned.
    */
  override def register(core: Int, name: String): Try[Register] = {
    Failure(new UnsupportedOperationException("not supported in TestProcessor"))
  }

  /**
    * Sets the register of the processor to the given content.
    *
    * The returned register (in case of Success(_)) contains the new register content. Failure is returned in case that
    *  - the processor is running and therefore the register cannot be set
    *  - the register is unknown to the processor
    *  - the value of the register is invalid
    *
    * @param register the register to set
    * @return Success with the new register value or Failure if the register cannot be set.
    */
  override def register(register: Register): Try[Register] = {
    Failure(new UnsupportedOperationException("not supported in TestProcessor"))
  }

  /**
    * Calculates the start address for a ROM/BIOS based on the size. The start address may be constant or dynamic
    * depending on the size (e.g. for x86). The start address is not necessarily the start address for the processor.
    * The start address is None if the ROM/BIOS is too large for the processor.
    * @return the start address for the ROM/BIOS depending on the architecture
    */
  override def calculateRomStart(romSize: Long): Option[Long] = if (romSize < maxMemory) Some(0) else None

  /**
    * Reset the processor and start it new.
    */
  override def reset(): Unit = {}

  /**
    * Do a single step of the processor (normally execute the next instruction).
    */
  override def step(): Unit = {}

}
