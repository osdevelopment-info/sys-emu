/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.system

import java.util.UUID
import org.specs2.mutable

class SystemsUnitSpec extends mutable.Specification {

  "Systems should" >> {
    "when adding" >> {
      "accept a new system" >> {
        val uuid = UUID.randomUUID
        val system = Some(new System(uuid))
        Systems.add(system) must_== true
      }
      "deny a known system" >> {
        val uuid = UUID.randomUUID
        val system = Some(new System(uuid))
        Systems.add(system)
        Systems.add(system) must_== false
      }
      "deny a system if it is not a system" >> {
        Systems.add(None) must_== false
      }
    }
    "when removing" >> {
      "delete a known system" >> {
        val uuid = UUID.randomUUID
        val system = Some(new System(uuid))
        Systems.add(system)
        Systems.remove(uuid) must_== true
      }
      "indicate an unknown system" >> {
        Systems.remove(UUID.fromString("00000000-0000-0000-0000-000000000000")) must_== false
      }
    }
  }

}
