/*
 * sys-emu - A system emulator for tutorials
 * Copyright (C) 2018 - 2019 osdevelopment-info
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package info.osdevelopment.sysemu.system

import info.osdevelopment.sysemu.memory.ReadWriteMemory
import info.osdevelopment.sysemu.processor.IllegalMemoryLayoutException
import info.osdevelopment.sysemu.support.Utilities._
import java.io.File
import org.specs2._

class SystemConfigUnitSpec extends mutable.Specification {

  "A system config" >> {
    "should load a HOCON file" >> {
      new SystemConfig(Some(new File("src/test/resources/config/unknown-system.conf")))
      success
    }
    "should not fail on a missing file" >> {
      new SystemConfig(Some(new File("invalid.conf")))
      success
    }
    "when loading a HOCON file" >> {
      "should read the CPU" >> {
        val systemConfig = new SystemConfig(Some(new File("src/test/resources/config/unknown-system.conf")))
        systemConfig.cpu must beSome("68000")
      }
      "should read the CPU count" >> {
        val systemConfig = new SystemConfig(Some(new File("src/test/resources/config/unknown-system.conf")))
        systemConfig.cpuCount must_== 3
      }
    }
    "when loading an empty HOCON file" >> {
      "should return the default CPU" >> {
        val systemConfig = new SystemConfig(Some(new File("src/test/resources/config/empty-system.conf")))
        systemConfig.cpu must beSome("8086")
      }
      "should read the CPU count" >> {
        val systemConfig = new SystemConfig(Some(new File("src/test/resources/config/empty-system.conf")))
        systemConfig.cpuCount must_== 1
      }
    }
    "when trying to load a not existing file" >> {
      "should return the uninitialized CPU" >> {
        val systemConfig = new SystemConfig(Some(new File("invalid.conf")))
        systemConfig.cpu must beNone
      }
      "should read no CPU count" >> {
        val systemConfig = new SystemConfig(Some(new File("invalid.conf")))
        systemConfig.cpuCount must_== 0
      }
    }
    "when creating from scratch" >> {
      "should have no CPU by default" >> {
        val systemConfig = new SystemConfig()
        systemConfig.cpu must beNone
      }
      "should have the CPU set" >> {
        val systemConfig = new SystemConfig()
        systemConfig.cpu = Some("Z8")
        systemConfig.cpu must beSome("Z8")
      }
      "should have no CPU when set to null" >> {
        val systemConfig = new SystemConfig()
        systemConfig.cpu = Some("Z8")
        systemConfig.cpu = None
        systemConfig.cpu must beNone
      }
      "should have 0 CPU count by default" >> {
        val systemConfig = new SystemConfig()
        systemConfig.cpuCount must_== 0
      }
      "should have the CPU count set" >> {
        val systemConfig = new SystemConfig()
        systemConfig.cpuCount = 16
        systemConfig.cpuCount must_== 16
      }
    }
    "when adding memory" >> {
      "should accept one memory" >> {
        val config = new SystemConfig
        val memory = ReadWriteMemory(512.Ki)
        memory must beSuccessfulTry
        config.addMemory(0x00000, memory.get)
        success
      }
      "should accept two non-overlapping memories" >> {
        val config = new SystemConfig
        val memory1 = ReadWriteMemory(512.Ki)
        memory1 must beSuccessfulTry
        val memory2 = ReadWriteMemory(512.Ki)
        memory2 must beSuccessfulTry
        config.addMemory(0x00000, memory1.get)
        config.addMemory(0x80000, memory2.get)
        success
      }
      "should not accept two overlapping memories (higher added last)" >> {
        val config = new SystemConfig
        val memory1 = ReadWriteMemory(512.Ki)
        memory1 must beSuccessfulTry
        val memory2 = ReadWriteMemory(512.Ki)
        memory2 must beSuccessfulTry
        config.addMemory(0x00000, memory1.get)
        config.addMemory(0x7ffff, memory2.get) must throwAn[IllegalMemoryLayoutException]
      }
      "should not accept two overlapping memories (higher added first)" >> {
        val config = new SystemConfig
        val memory1 = ReadWriteMemory(512.Ki)
        memory1 must beSuccessfulTry
        val memory2 = ReadWriteMemory(512.Ki)
        memory2 must beSuccessfulTry
        config.addMemory(0x7ffff, memory2.get)
        config.addMemory(0x00000, memory1.get) must throwAn[IllegalMemoryLayoutException]
      }
      "should return an added memory" >> {
        val config = new SystemConfig
        val memory = ReadWriteMemory(512.Ki)
        memory must beSuccessfulTry
        config.addMemory(0x00000, memory.get)
        config.memory must havePair(0x00000, memory.get)
      }
    }
  }

}
