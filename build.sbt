name := "sys-emu"

organization := "info.osdevelopment"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.8"

libraryDependencies += "com.github.swagger-akka-http" %% "swagger-akka-http" % "2.0.2"
libraryDependencies += "com.typesafe" % "config" % "1.3.3"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.21"
libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.7"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % "2.5.21"
libraryDependencies += "com.typesafe.akka" %% "akka-stream" % "2.5.21"
libraryDependencies += "jakarta.ws.rs" % "jakarta.ws.rs-api" % "2.1.5"
libraryDependencies += "org.slf4j" % "slf4j-jdk14" % "1.7.25"
libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.5.21" % "test"
libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % "10.1.7" % "test"
libraryDependencies += "org.specs2" %% "specs2-core" % "4.2.0" % "test"
libraryDependencies += "org.specs2" %% "specs2-mock" % "4.2.0" % "test"

scalacOptions += "-feature"
scalacOptions += "-deprecation"

autoAPIMappings := true

test in assembly := {}

assemblyMergeStrategy in assembly := {
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
